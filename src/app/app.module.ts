import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { CubeComponent } from "./components/cube/cube.component";
import { MapComponent } from "./components/map/map.component";

@NgModule({
  declarations: [AppComponent, CubeComponent],
  imports: [BrowserModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
