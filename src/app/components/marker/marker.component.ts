import { Vector3 } from "three";
import * as THREE from "three-full";

export class Marker {
  scene: THREE.Scene;
  model: any;
  locationX: number;
  locationY: number;
  scale: number;
  constructor(
    scene: THREE.Scene,
    locationX: number,
    locationY: number,
    scale: number
  ) {
    this.scene = scene;
    this.locationX = locationX;
    this.locationY = locationY;
    this.scale = scale;
    this.init();
  }

  init() {
    const objLoader = new THREE.OBJLoader();
    const mtlLoader = new THREE.MTLLoader();
    mtlLoader.load("/assets/marker.mtl", (mtl) => {
      mtl.preload();
      objLoader.setMaterials(mtl);
      objLoader.load("/assets/marker.obj", (root) => {
        this.model = root;
        this.model.castShadow = true;
        root.scale.set(this.scale, this.scale, this.scale);
        root.position.set(this.locationX, this.locationY, 3);
        //root.rotateOnAxis(new Vector3(1, 0, 0), THREE.MathUtils.degToRad(20));
        this.scene.add(this.model);
      });
    });
  }

  rotateY() {}

  translateX(translation: number) {
    const rotationSpeed = Date.now() * 0.001;
    const rotationLength = translation / 2;
    this.model.position.x =
      Math.sin(rotationSpeed / 2) / rotationLength + this.locationX;
  }
  rotateX() {}

  animate() {}
}
