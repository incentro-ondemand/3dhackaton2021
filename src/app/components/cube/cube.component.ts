import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  ViewChild,
} from "@angular/core";
import { Vector3 } from "three";
import * as THREE from "three-full";
import { MapComponent } from "../map/map.component";
import { Marker } from "../marker/marker.component";

@Component({
  selector: "app-cube",
  templateUrl: "./cube.component.html",
  styleUrls: ["./cube.component.scss"],
})
export class CubeComponent implements AfterViewInit {
  @ViewChild("canvas", { static: false })
  private canvasRef: ElementRef;
  //* Cube Properties
  @Input() public rotationSpeedX: number = 0;
  @Input() public rotationSpeedY: number = -0.01;
  @Input() public size: number = 200;
  @Input() public texture: string = "/assets/world.jpg";
  //* Stage Properties
  @Input() public cameraZ: number = 400;
  @Input() public fieldOfView: number = 1;
  @Input("nearClipping") public nearClippingPlane: number = 1;
  @Input("farClipping") public farClippingPlane: number = 1000;
  //? Helper Properties (Private Properties);
  private camera!: THREE.PerspectiveCamera;
  nlMap: MapComponent;
  ESMarker: Marker;
  KYMarker: Marker;

  private intersectedPoint = new THREE.Vector3();
  private raycaster = new THREE.Raycaster();
  stopAnimation;

  private get canvas(): HTMLCanvasElement {
    return this.canvasRef.nativeElement;
  }
  private loader = new THREE.TextureLoader();
  private geometry = new THREE.SphereGeometry(2.3, 32, 32);
  private material = new THREE.MeshStandardMaterial({
    map: this.loader.load(this.texture),
  });
  private sphere: THREE.Mesh = new THREE.Mesh(this.geometry, this.material);

  private NLMarker: Marker;
  private renderer!: THREE.WebGLRenderer;
  private scene!: THREE.Scene;

  constructor() {}

  ngAfterViewInit() {
    this.createScene();
    this.startRenderingLoop();
  }
  /**
   *Animate the cube
   *
   * @private
   * @memberof CubeComponent
   */
  private animateSphere() {
    const rotationSpeed = Date.now() * 0.001;
    const rotationLength = 5;
    const translation = 4.8;

    this.sphere.rotation.x += this.rotationSpeedX;
    this.sphere.rotation.y =
      Math.sin(rotationSpeed / 2) / rotationLength + translation;

    this.ESMarker.translateX(translation);
    this.NLMarker.translateX(translation);
    this.KYMarker.translateX(translation);
  }

  /**
   * Create the scene
   *
   * @private
   * @memberof CubeComponent
   */
  private createScene() {
    //* Scene
    this.scene = new THREE.Scene();
    this.scene.background = new THREE.Color(0x000000);
    //markers
    this.ESMarker = new Marker(this.scene, -0.4, 1, 0.2);
    this.NLMarker = new Marker(this.scene, 0, 1.3, 0.2);
    this.KYMarker = new Marker(this.scene, 1.3, -0.2, 0.2);
    //light
    const spotLight = new THREE.DirectionalLight(0xffffff, 1, 100);
    spotLight.position.set(50, 30, 50);
    spotLight.castShadow = true;
    this.scene.add(spotLight);
    //objects
    this.createStars(10, 5);
    this.sphere.rotation.y = THREE.MathUtils.degToRad(-85);
    this.sphere.castShadow = true;
    this.sphere.receiveShadow = true;
    this.scene.add(this.sphere);

    //*Camera
    let aspectRatio = this.getAspectRatio();
    this.camera = new THREE.PerspectiveCamera(
      this.fieldOfView,
      aspectRatio,
      this.nearClippingPlane,
      this.farClippingPlane
    );
    this.camera.position.z = this.cameraZ;

  }
  private getAspectRatio() {
    return this.canvas.clientWidth / this.canvas.clientHeight;
  }

  private createStars(radius, segments) {
    const scene = this.scene;
    const texLoader = new THREE.TextureLoader();
    texLoader.load(
      // resource URL
      "/assets/galaxy_starfield.png",
      // onLoad callback
      function (texture) {
        // in this example we create the material when the texture is loaded
        scene.add(
          new THREE.Mesh(
            new THREE.SphereGeometry(radius, segments, segments),
            new THREE.MeshBasicMaterial({
              map: texture,
              side: THREE.BackSide,
            })
          )
        );
      },
      // onProgress callback currently not supported
      undefined,
      // onError callback
      function (err) {
        console.error("An error happened.");
      }
    );
  }
  /**
   * Start the rendering loop
   *
   * @private
   * @memberof CubeComponent
   */
  private startRenderingLoop() {
    //* Renderer
    // Use canvas element in template
    this.renderer = new THREE.WebGLRenderer({ canvas: this.canvas });
    this.renderer.setPixelRatio(devicePixelRatio);
    this.renderer.setSize(this.canvas.clientWidth, this.canvas.clientHeight);
    this.renderer.shadowMap.enabled = true;
    this.renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    let component: CubeComponent = this;
    (function render() {
      requestAnimationFrame(render);
      component.animateSphere();
      if (component.nlMap) {
        component.nlMap.animate();
      }
      //animate markers
      component.NLMarker.animate();
      component.renderer.render(component.scene, component.camera);
    })();
  }

  public onClick(event) {
    event.preventDefault();
    const vector = new THREE.Vector3(
      (event.clientX / window.innerWidth) * 2 - 1,
      -(event.clientY / window.innerHeight) * 2 + 1,
      0.5
    );
    this.raycaster.setFromCamera(vector, this.camera);
    const raycaster = new THREE.Raycaster(
      this.camera.position,
      vector.sub(this.camera.position).normalize()
    );
    const intersects = raycaster.intersectObjects(this.scene.children);
    if (intersects.length > 0) {

      //nl map
      this.nlMap = new MapComponent(
        this.scene,
        "/assets/Maps/Netherlands/Netherlands"
      );

      this.scene.remove(this.sphere);
      this.scene.remove(this.NLMarker.model);
      this.scene.remove(this.ESMarker.model);
      this.scene.remove(this.KYMarker.model);


      this.intersectedPoint = intersects[0].point;
      console.log(intersects[0].object);
      this.camera.position.x -= this.intersectedPoint.x;
      this.camera.position.y -= this.intersectedPoint.y;
      this.camera.position.z -= this.intersectedPoint.z;
      // this.stopAnimation = true;
      intersects[0].object.material.color.setHex(0xffffff);
    } else {
      this.camera.position.x += this.intersectedPoint.x;
      this.camera.position.y += this.intersectedPoint.y;
      this.camera.position.z += this.intersectedPoint.z;
    }
  }
}
