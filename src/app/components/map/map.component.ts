import { Vector3 } from "three";
import * as THREE from "three-full";

export class MapComponent {
  scene: THREE.Scene;
  model: any;
  map: string;
  constructor(scene: THREE.Scene, map: string) {
    this.scene = scene;
    this.map = map;
    this.init();
  }

  init() {
    const objLoader = new THREE.OBJLoader();
    const mtlLoader = new THREE.MTLLoader();
    mtlLoader.load(this.map + ".mtl", (mtl) => {
      mtl.preload();
      objLoader.setMaterials(mtl);
      objLoader.load(this.map + ".obj", (root) => {
        this.model = root;
        root.scale.set(5, 5, 5);
        root.rotateOnAxis(new Vector3(1, 0, 0), THREE.MathUtils.degToRad(20));
        this.scene.add(root);
      });
    });
  }

  animate() {
    const rotationSpeed = Date.now() * 0.001;
    const rotationLength = 5;
    const translation = 4.8;

    this.model.rotation.x += 0;
    const scale = Math.sin(rotationSpeed / 2) / rotationLength + translation;
    this.model.scale.set(scale, scale, scale);
  }
}
